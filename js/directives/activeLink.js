App.directive('activeLink', ['$location', function($location) {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      var path = attrs.activeLink ? 'activeLink' : 'href';
      var target = angular.isDefined(attrs.activeLinkParent) ? elem.parent() : elem;
      var disabled = angular.isDefined(attrs.activeLinkDisabled) ? true : false;
      var nested = angular.isDefined(attrs.activeLinkNested) ? true : false;

      function inPath(needle, haystack) {
        if (angular.isArray(needle)) {
          for(var i in needle) {
            var current = (haystack == needle[i]);
            if (nested) {
              current |= (haystack.indexOf(needle[i] + '/') === 0);
            }

            if (current) return current;
          }
          return false;
        } else {
          var current2 = (haystack == needle);
          if (nested) {
            current2 |= (haystack.indexOf(needle + '/') === 0);
          }
          return current2;
        }
      }

      function toggleClass(linkPath, locationPath) {
        // remove hash prefix and trailing slashes
        if (linkPath) {
          if (linkPath.indexOf(',') === -1) {
            linkPath = linkPath.replace(/^#!/, '').replace(/\/+$/, '');
          } else {
            var linkPaths = [];
            angular.forEach(linkPath.split(","), function(linkPath, i) {
              this.push(linkPath.replace(/^#!/, '').replace(/\/+$/, ''));
            }, linkPaths);
            linkPath = linkPaths;
          }
        }

        locationPath = locationPath.replace(/\/+$/, '');

        if (linkPath && inPath(linkPath, locationPath)) {
          target.addClass('active');
          if (disabled) {
            target.removeClass('disabled');
          }
        } else {
          target.removeClass('active');
          if (disabled) {
            target.addClass('disabled');
          }
        }
      }

      // watch if attribute value changes / evaluated
      attrs.$observe(path, function(linkPath) {
        toggleClass(linkPath, $location.path());
      });

      // watch if location changes
      scope.$watch(
        function() {
          return $location.path();
        },
        function(newPath) {
          toggleClass(attrs[path], newPath);
        }
      );
    }
  };
}]);
