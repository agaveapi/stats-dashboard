angular.module('ui.knob', []).directive('knob', ['$timeout', function($timeout) {
    'use strict';

    return {
        restrict: 'EA',
        replace: true,
        template: '<input value="{{ knobData }}"/>',
        scope: {
            knobData: '=',
            knobOptions: '='
        },
        link: function($scope, $element) {
            var knobInit = $scope.knobOptions || {};

            // knobInit.release = function(newValue) {
            //     $timeout(function() {
            //         // $scope.knobData = newValue;
            //         $scope.$apply();
            //     });
            // };

            $scope.$watch('knobData', function(newValue, oldValue) {
              console.log("Value changed from " + oldValue + " to " + newValue);
                if (newValue != oldValue) {
                    $($element)
                        .trigger('configure', $scope.knobOptions)
                        .animate({
                              value: parseInt(newValue)
                            }, {
                              duration: 1000,
                              easing: 'swing',
                              progress: function () {
                                  $(this).val(Math.ceil(this.value)).trigger('change');
                              }
                            });
                }
            });

            $($element).val($scope.knobData).knob(knobInit);
        }
    };
}]);
