App.controller('JobsController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.hourlyJobs = { data: [], options: {} };
  $scope.dailyJobs = { data: [], options: {} };
  $scope.defaultColors = Commons.shuffle(['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67']);
  $scope.summary = {
    averages: {
      runTime: 0,
      cores: 0,
      jobsPerUser: 0
    }
  };

  $scope.knobs = {
    runTime: {
      value: $scope.summary.averages.runTime,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: ($scope.summary.averages.runTime > 10 ? $scope.summary.averages.runTime * 1.5 : 10),
        angleArc: 250,
        angleOffset: -125,
        fgColor: $scope.defaultColors[0]
      }
    },
    cores: {
      value: $scope.summary.averages.cores,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: ($scope.summary.averages.cores > 10 ? $scope.summary.averages.cores * 1.25 : 10),
        angleArc: 250,
        angleOffset: -125,
        fgColor: $scope.defaultColors[1]
      }
    },
    jobsPerUser: {
      value: $scope.summary.averages.jobsPerUser,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: ($scope.summary.averages.jobsPerUser > 10 ? $scope.summary.averages.jobsPerUser * 1.25 : 10),
        angleArc: 250,
        angleOffset: -125,
        fgColor: $scope.defaultColors[2]
      }
    }
  };

  $scope.updateDailyJobSubmissions = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[0].values, function(value) {
      data.push([moment("2014-01-01 " + value.name), value.count]);
    });
    $scope.dailyJobSubmissions.data = [data];
    $scope.dailyJobSubmissions.options = {
      xaxis: {
        mode: "time",
        timeformat: "%I:00%p"
      },
      grid: {
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateHourlyJobSubmissions = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[1].values, function(value) {
      data.push([moment("2014-01-01 " + value.name + ":00:00"), value.count]);
    });
    $scope.hourlyJobSubmissions.data = [data];
    $scope.hourlyJobSubmissions.options = {
      xaxis: {
        mode: "time",
        timeformat: "%I:00%p"
      },
      grid: {
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  var usagePromise = agave.usage.jobs.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;
      $scope.updateDailyJobs();
      $scope.updateHourlyJobs();
      $scope.updateJobMap();
      $scope.updateJobSchedulers();
      $scope.updateJobExecutionTypes();

      $scope.summary.averages.runTime = Math.floor($scope.summary.averages.runTime/60);
      $scope.summary.averages.cores = Math.ceil($scope.summary.averages.cores);
      $scope.summary.averages.jobsPerUser = Math.ceil($scope.summary.averages.jobsPerUser);
      console.log($scope.summary.averages.runTime);

      $scope.knobs.runTime.options.max = ($scope.summary.averages.runTime > 720 ? $scope.summary.averages.runTime * 1.5 : 720);
      $scope.knobs.cores.options.max = ($scope.summary.averages.cores > 32 ? $scope.summary.averages.cores * 1.25 : 32);
      $scope.knobs.jobsPerUser.options.max = ($scope.summary.averages.jobsPerUser > 100 ? $scope.summary.averages.jobsPerUser * 1.25 : 100);

      setTimeout(function() {

        $("#dailyJobs").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(moment(item.datapoint[0]).format("MMM Do") + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' jobs')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
        $("#hourlyJobs").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(moment(item.datapoint[0]).format("h:mm a") + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' jobs')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
        $("#jobExecutionTypes, #jobSchedulers").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(item.series.label + " - " + $filter('number')(item.datapoint[0], 0) + '%')
                .css({top: pos.pageY+5, left: pos.pageX+5})
                .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
      }, 200);
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.updateDailyJobs = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[1].values, function(value) {
      data.push([moment(value.name), value.count]);
    });
    $scope.dailyJobs.data = [data];
    $scope.dailyJobs.options = {
      colors: Commons.pick(3, $scope.defaultColors),
      canvas: true,
      series: {
        points: { show: true },
        lines: { show: true, fill: true }
      },
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [1, "day"],
        timeformat: "%m/%d/%Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateHourlyJobs = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[0].values, function(value) {
      data.push([moment("2014-01-01 " + value.name + ":00:00"), value.count]);
    });
    $scope.hourlyJobs.data = [data];
    $scope.hourlyJobs.options = {
      colors: Commons.pick(3, $scope.defaultColors),
      canvas: true,
      series: {
        points: { show: true },
        lines: { show: true, fill: true }
      },
      xaxis: {
        mode: "time",
        timeformat: "%I:00%p",
        tickSize: [2, 'hour'],
        minTickSize: [1, "hour"]
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateJobMap = function() {
    var countries = {};
    angular.forEach($scope.summary.locations, function(location, ip) {
      var code = Commons.countries[location.country];
      if (code) {
        code = code.toLowerCase()
        if (countries[code]) {
          countries[code] += location.requests;
        } else {
          countries[code] = location.requests;
        }
      }
    });

    var max = 0,
    min = Number.MAX_VALUE,
    cc,
    startColor = [200, 238, 255],
    endColor = [0, 100, 145],
    colors = {},
    hex;

    //find maximum and minimum values
    for (cc in countries)
    {
      if (countries[cc] > max)
      {
        max = countries[cc];
      }
      if (countries[cc] < min)
      {
        min = countries[cc];
      }
    }

    //set colors according to values of GDP
    for (cc in countries) {
      if (countries[cc] > 0) {
        colors[cc] = '#';
        for (var i = 0; i<3; i++) {
          hex = Math.round(startColor[i] + (endColor[i] - startColor[i]) * (countries[cc] / (max - min))).toString(16);
          if (hex.length == 1) {
            hex = '0'+hex;
          }
          colors[cc] += (hex.length == 1 ? '0' : '') + hex;
        }
      }
    }

    $('#vmap').vectorMap({
      map: 'world_en',
      // colors: colors,
      backgroundColor: '#37455f',
      color: '#f3f3f3',
      hoverOpacity: 0.7,
      selectedColor: '#666666',
      // hoverColor: false,
      enableZoom: true,
      showTooltip: true,
      values: countries,
      scaleColors: ['#C8EEFF', '#006491'],
      normalizeFunction: 'polynomial',
      onLabelShow: function(event, label, code)
      {
        if (countries[code]) {
          label.text(label.text() + ' - ' + $filter('humanNumber')(countries[code], 0, true) );
        } else {
          event.preventDefault();
        }
      },
      onRegionClick: function(event, code) {
        if (!$(event.currentTarget).data('mapObject').isMoving) {
          e.preventDefault();
        }
      }
    });
  };

  $scope.updateJobSchedulers = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[1].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.jobSchedulers.data = data;
    $scope.jobSchedulers.options = {
      colors: Commons.shuffle($scope.defaultColors),
      canvas: true,
      series: {
        pie: {
          show: true,
          innerRadius: 0.5,
          combine: {
            color: '#999',
            threshold: 0.05
          }
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateJobExecutionTypes = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[2].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.jobExecutionTypes.data = data;
    $scope.jobExecutionTypes.options = {
      colors: Commons.shuffle($scope.defaultColors).reverse(),
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
          // label: function (label, series) {
          //   return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
          // }
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

});
