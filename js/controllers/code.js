App.controller('CodeController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.myData = [];
  $scope.myChartOptions = {};

  $scope.statuses = {
    up: {
      icon: 'fa-thumbs-o-up',
      values: ['Up', 'Bueno', 'Good', 'Rockin', 'Cruising', 'Awesome', 'Super', 'Excellent', 'Joyful', 'Fierce']
    },
    down: {
      icon: 'fa-thumbs-o-down',
      values: ['Down', 'Borked', 'Struggling', 'Help', 'On strike', 'Asleep', 'Mid-life crisis', 'Bitter', 'Angstful', 'Sad', 'Angry']
    },
    building: {
      icon: 'fa-spin fa-spinner',
      values: ['Building']
    }
  };

  var usagePromise = agave.usage.code.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;
      var statusCode = Object.keys($scope.statuses)[Commons.randomInt(0, 2)];
      var statusValues = $scope.statuses[statusCode].values;
      var svLen = Commons.randomInt(0, statusValues.length - 1);
      $scope.summaryStatus = statusValues[svLen];
      $scope.summaryStatusIcon = $scope.statuses[statusCode].icon
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};
});
