App.controller('AppsController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.appRegistrations = { data: [], options: {} };
  $scope.appCategories = { data: [], options: {} };
  $scope.appUses = { data: [], options: {} };
  $scope.defaultColors = Commons.shuffle(['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67', '#5bc0de']);

  var usagePromise = agave.usage.apps.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;
      $scope.updateAppRegistrations();
      $scope.updateAppUses();
      $scope.updateAppCategories();

      setTimeout(function() {
        $("#registrations").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(moment(item.datapoint[0]).format("MMM Do") + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' app registrations')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
        $("#appUses, #appCategories").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(item.series.label + " - " + $filter('number')(item.datapoint[0], 0) + '%')
            .css({top: pos.pageY+5, left: pos.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
      }, 100);
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};

  $scope.updateAppRegistrations = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[1].values, function(value) {
      data.push([moment(value.name), value.count]);
    });
    $scope.appRegistrations.data = [data];
    $scope.appRegistrations.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        points: { show: true },
        lines: { show: true, fill: true }
      },
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [1, "day"],
        timeformat: "%m/%d/%Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateAppCategories = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[0].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.appCategories.data = data;
    $scope.appCategories.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          innerRadius: 0.5,
          combine: {
            color: '#999',
            threshold: 0.05
          }
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateAppUses = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[1].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.appUses.data = data;
    $scope.appUses.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
          // label: function (label, series) {
          //   return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + Math.round(series.percent) + "%</div>";
          // }
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

});
