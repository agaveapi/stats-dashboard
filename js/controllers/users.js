App.controller('UsersController', function ($scope, $state, $sanitize, $filter, agave, toastr) {
  $scope.myData = [];
  $scope.myChartOptions = {};

  var usagePromise = agave.usage.users.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};
});
