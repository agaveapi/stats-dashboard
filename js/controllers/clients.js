App.controller('ClientsController', function ($scope, $state, $sanitize, $filter, agave, toastr, Commons) {

  $scope.topClients = { data: [], options: {} };
  $scope.clientsByUsers = { data: [], options: {} };
  $scope.clientsByTenants = { data: [], options: {} };
  $scope.clientsByTraffic = { data: [], options: {} };
  $scope.clientRegistrations = { data: [], options: {} };
  $scope.defaultColors = ['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67', '#594857', '#5bc0de'];

  $scope.updateTopClients = function() {
    var data = [];
    var ticks = [];
    //var utcindex = 0;
    angular.forEach($scope.summary.traffic[0].values, function(value, yearmonth) {
      var eventData = moment(yearmonth + '-01 00:00:00');
      data.push({label: moment(yearmonth + '-01').format('MMM YYYY'), data: [[eventData, value]]});
    });

    $scope.topClients.data = data;
    $scope.topClients.options = {
      colors: Commons.pick(3, $scope.defaultColors),
      canvas: true,
      series: {
        //points: { show: false },
        //lines: { show: true, fill: true },
        bars: { show: true, fill: true }
      },
      bars: {
        barWidth: 20* 24 * 60 * 60 * 1000,
        align: 'center',
        horizontal: false
      },
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [30, "day"],
        timeformat: "%b %Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      },
      legend: {
        show: false
      }
    };
  };

  $scope.updateClientRegistrations = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[1].values, function(value, yearmonth) {
      var eventData = moment(yearmonth + '-01 00:00:00');
      data.push([eventData, value]);
    });
    $scope.clientRegistrations.data = [data];
    $scope.clientRegistrations.options = {
      colors: Commons.pick(3, Commons.shuffle($scope.defaultColors)),
      canvas: true,
      series: {
        lines: {
          show: true,
          fill: true,
          zero: true,
          lineWidth: 3
        }
      },
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [30, "day"],
        timeformat: "%b %Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      },
      legend: {
        show: false
      }
    };
  };

  //   options: {
  //     series: {
  //       pie: {
  //         show: true,
  //         combine: {
  //           color: '#999',
  //           threshold: 0.1
  //         }
  //       }
  //     },
  //     legend: {
  //       show: false
  //     }
  //   }
  // };

  var usagePromise = agave.usage.clients.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;

      $scope.updateTopClients();
      $scope.updateClientRegistrations();

      $scope.updateClientsByTenants();
      $scope.updateClientsByTraffic();
      $scope.updateClientsByUsers();

      setTimeout(function() {

        $("#clientsByTenant, #clientsByTraffic, #clientsByUsers").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(item.series.label + " - " + $filter('number')(item.datapoint[0], 0) + '%')
                .css({top: pos.pageY+5, left: pos.pageX+5})
                .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });

        $("#clientRegistrations").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(item.series.label + " - " + $filter('humanNumber')(item.datapoint[1],1) + ' registrations')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });

        $("#topClients").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(item.series.label + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' active applications')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
      }, 200);
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};

  $scope.updateClientsByTraffic = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[1].values, function(value, index) {
      data.push({label: value.tenant + ": " + value.name, data: value.count});
    });
    $scope.clientsByTraffic.data = data;
    $scope.clientsByTraffic.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
        }
      },
      legend: {
        show: true,
        position: "sw"
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateClientsByTenants = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[0].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.clientsByTenants.data = data;
    $scope.clientsByTenants.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.001
          },
        }
      },
      legend: {
        show: true,
        position: "sw"
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateClientsByUsers = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[2].values, function(value, index) {
      data.push({label:value.tenant + ": " + value.name, data: value.count});
    });
    $scope.clientsByUsers.data = data;
    $scope.clientsByUsers.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
        }
      },
      legend: {
        show: true,
        position: "se"
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

});
