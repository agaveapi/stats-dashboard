App.controller('TrafficController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.hourlyTraffic = { data: [], options: {} };
  $scope.dailyTraffic = { data: [], options: {} };
  $scope.defaultColors = ['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67'];


  var usagePromise = agave.usage.traffic.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;
      $scope.updateDailyTraffic();
      //$scope.updateHourlyTraffic();

      setTimeout(function() {
        $("#dailyTraffic").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(moment(item.datapoint[0]).format("MMM Do") + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' requests')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
        $("#hourlyTraffic").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(moment(item.datapoint[0]).format("h:mm a") + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' requests')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
      }, 100);
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};

  $scope.updateDailyTraffic = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[1].values, function(value) {
      data.push([moment(value.name), value.count]);
    });
    $scope.dailyTraffic.data = [data];
    $scope.dailyTraffic.options = {
      colors: Commons.pick(3, $scope.defaultColors),
      canvas: true,
      series: {
        points: { show: true },
        lines: { show: true, fill: true }
      },
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [30, "day"],
        timeformat: "%m/%d/%Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  //$scope.updateHourlyTraffic = function() {
  //  var data = [];
  //  angular.forEach($scope.summary.traffic[0].values, function(value) {
  //    data.push([moment("2014-01-01 " + value.name + ":00:00"), value.count]);
  //  });
  //  $scope.hourlyTraffic.data = [data];
  //  $scope.hourlyTraffic.options = {
  //    colors: Commons.pick(3, $scope.defaultColors),
  //    canvas: true,
  //    series: {
  //      points: { show: true },
  //      lines: { show: true, fill: true }
  //    },
  //    xaxis: {
  //      mode: "time",
  //      timeformat: "%I:00%p",
  //      tickSize: [2, 'hour'],
  //      minTickSize: [1, "hour"]
  //    },
  //    yaxis: {
  //      tickFormatter: function(n) {
  //        return $filter('humanNumber')(n, 0);
  //      }
  //    },
  //    grid: {
  //      hoverable: true,
  //      clickable: true,
  //      backgroundColor: { colors: [ "#fff", "#fff" ] },
  //      borderWidth: {
  //        top: 0,
  //        right: 0,
  //        bottom: 0,
  //        left: 0
  //      }
  //    }
  //  };
  //};

  // $("<div id='tooltip'></div>").css({
  //   position: "absolute",
  //   display: "none",
  //   border: "1px solid #656565",
  //   padding: "2px 5px",
  //   "background-color": "#f2f2f2",
  //   color: "#656565",
  //   opacity: 0.80
  // }).appendTo("body");

});
