App.controller('SummaryController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.trafficData = { data: [], options: {} };
  $scope.defaultColors = ['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67', '#594857', '#5bc0de'];
  $scope.statuses = {
    UP: {
      icon: 'fa-thumbs-o-up',
      values: ['Up', 'Bueno', 'Good', 'Rockin', 'Cruising', 'Awesome', 'Super', 'Excellent', 'Joyful', 'Fierce']
    },
    DOWN: {
      icon: 'fa-thumbs-o-down',
      values: ['Down', 'Borked', 'Struggling', 'Help', 'On strike', 'Asleep', 'Mid-life crisis', 'Bitter', 'Angstful', 'Sad', 'Angry']
    },
    MAINTENANCE: {
      icon: 'fa-gamepad',
      values: ['In surgery', 'Maintenance', 'Taking a breather', 'Recharging', 'Tune-up', 'Oil change', "Back in a few", 'Vacation', 'On break', 'Pensive', 'Tired']
    },
    DEPLOYING: {
      icon: 'fa-spin fa-spinner',
      values: ['Growing', 'Moving', 'Bulking up', 'Growth spurt', 'Cowboy up', 'Eating Wheaties', 'Power nap', 'Power up', 'Manning up']
    }
  };

  var usagePromise = agave.usage.summary.getAll();

  usagePromise.then(
    function(response) {

      $scope.summary = response.data.result;
      //var statusCode = Object.keys($scope.statuses)[Commons.randomInt(0, 3)];
      var statusValues = $scope.statuses[$scope.summary.totals.status].values;
      var svLen = Commons.randomInt(0, statusValues.length - 1);
      $scope.summaryStatus = statusValues[svLen];
      $scope.summaryStatusIcon = $scope.statuses[$scope.summary.totals.status].icon

    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};

  agave.usage.traffic.getAll().then(
    function(response) {
      $scope.traffic = response.data.result;


       //$scope.updateTraffic();
       //
       //setTimeout(function() {
       //  $("#traffic").bind("plotclick", function (event, pos, item) {
       //    if (item) {
       //      $("#tooltip").html(moment(item.datapoint[0]).format("MMM Do") + " - " + $filter('humanNumber')(item.datapoint[1], 1) + ' requests')
       //      .css({top: item.pageY+5, left: item.pageX+5})
       //      .fadeIn(200);
       //    } else {
       //      $("#tooltip").hide();
       //    }
       //  });
       //}, 100);
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.updateTraffic = function() {
    var data = [];
    angular.forEach($scope.traffic.values, function(value) {
      data.push([moment(value.name), value.count]);
    });
    $scope.trafficData.data = [data];
    $scope.trafficData.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        points: { show: true },
        lines: { show: true }
      },
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [1, "day"],
        timeformat: "%m/%d/%Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };
});
