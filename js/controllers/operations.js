App.controller('OperationsController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.apiUptimes = { data:[], options:[] };
  $scope.defaultColors = Commons.shuffle(['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67', '#594857', '#5bc0de']);
  $scope.averageUptime = 0;
  $scope.summary = {};

  // $('.dial').knob({
  //   readOnly: true,
  //   thickness: 0.4,
  //   dynamicDraw: true,
  //   tickColorizeValues: true,
  //   // fgColor: ($scope.averageUptime < 96 ? '#E9662C' : ($scope.averageUptime < 99) ? '#87CEEB' : '#488C6C')
  // })
  //
  $scope.knobs = {
    averageUptime: {
      value: $scope.averageUptime,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: 100,
        width: "100%",
        height: 220,
        angleArc: 250,
        angleOffset: -125,
        fgColor: ($scope.averageUptime < 96 ? '#E9662C' : ($scope.averageUptime < 99) ? '#87CEEB' : '#488C6C')
      }
    }
  };

  $scope.updateApiUptimes = function() {

    var data = [];
    var ticks = [];
    var index = 0.0;
    var totalUptime = Number(0.0);
    angular.forEach($scope.summary.averages, function(uptime, apiName) {
      data.push({color: $scope.defaultColors[index], label: apiName, data: [[index, Number(uptime)]]});
      ticks.push([index, $filter('capitalize')(apiName)]);
      index++;
      totalUptime += Number(uptime);
    });

    $scope.averageUptime = Math.ceil(totalUptime / Number(index));

    $scope.apiUptimes.data = data;
    $scope.apiUptimes.options = {
      legend: false,
      grid: {
        hoverable: true,
        clickable: true,
        borderWidth: false,
      },
      series: {
        bars: {
          show: true,
          barWidth: 0.6,
          fill: 0.8,
          lineWidth: 1,
          align: "center"
        }
      },
      xaxis: {
        ticks: ticks
      },
      yaxis: {
        axisLabel: "Average Uptime",
        min: 90.0,
        max: 100.0,
        tickSize: 2,
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0) + '%';
        }
      }
    };
  };

  var usagePromise = agave.usage.operations.getAll();

  usagePromise.then(
    function(response) {
      $scope.summary = response.data.result;
      $scope.totalContainers = Commons.sum($scope.summary.leaders[0].values, 'count');
      $scope.updateApiUptimes();
      $scope.updateDatacenters();
      $scope.updateContainerDistribution();

      setTimeout(function() {

        // $('.dial')
        //   .trigger('configure', {
        //     fgColor: ($scope.averageUptime < 96 ? '#E9662C' : ($scope.averageUptime < 99) ? '#87CEEB' : '#488C6C')
        //   })
        //   .animate({
        //       value: parseInt($scope.averageUptime)
        //     }, {
        //       duration: 1000,
        //       easing: 'swing',
        //       progress: function () {
        //         $(this).val(Math.round(this.value)).trigger('change')
        //       }
        //     });

        $("#apiUptimes").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html($filter('capitalize')(item.series.label) + " - " + $filter('humanNumber')(item.datapoint[1], 1) + '% uptime')
            .css({top: item.pageY+5, left: item.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });

      }, 50);

    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.updateDatacenters = function() {
    var locations = {
      'tx': 'Texas Advanced Computing Center',
      'az': 'University of Arizona',
      'in': 'Indiana University',
      'ca': 'Microsft Azure - West US',
      'va': 'AWS - US East'
    };

    var colors = {
      'tx': '#666666',
      'az': '#666666',
      'in': '#666666',
      'ca': '#666666',
      'va': '#666666'
    };


    $('#vmap').vectorMap({
      map: 'usa_en',
      backgroundColor: '#37455f',
      colors: colors,
      //color: '#f3f3f3',
      hoverOpacity: 0.7,
      selectedColor: '#666666',
      hoverColor: false,
      enableZoom: true,
      showTooltip: true,
      // values: locations,
      scaleColors: '#666666',
      normalizeFunction: 'polynomial',
      onLabelShow: function(event, label, code)
      {
        if (locations[code]) {
          label.text(locations[code]);
        } else {
          event.preventDefault();
        }
      },
      onRegionClick: function(event, code) {
        if (!$(event.currentTarget).data('mapObject').isMoving) {
          e.preventDefault();
        }
      }
    });
  };

  $scope.updateContainerDistribution = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[0].values, function(value, index) {
      data.push({label:$filter('capitalize')(value.name), data: value.count});
    });
    $scope.containerDistribution.data = data;
    $scope.containerDistribution.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0
          },
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: false,
      }
    };
  };

});
