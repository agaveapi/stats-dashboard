App.controller('DataController', function ($scope, $state, $sanitize, $filter, agave, Commons, toastr) {
  $scope.dailyTransfers = [];
  $scope.hourlyTransfers = {};
  $scope.defaultColors = Commons.shuffle(['#BF4346', '#E9662C', '#488C6C', '#F2994B', '#f0AD4E', '#0A819C', '#9351AD', '#BF3773', '#4B5D67']);
  $scope.summary = {
    averages: {
      bytesPerTransfer: 0,
      filesPerTransfer: 0,
      transfersPerUser: 0
    }
  };

  $scope.knobs = {
    bytesPerTransfer: {
      value: $scope.summary.averages.bytesPerTransfer,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: ($scope.summary.averages.bytesPerTransfer > 5 ? $scope.summary.averages.bytesPerTransfer * 1.25 : 5),
        angleArc: 250,
        angleOffset: -125,
        fgColor: $scope.defaultColors[0]
      }
    },
    filesPerTransfer: {
      value: $scope.summary.averages.filesPerTransfer,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: ($scope.summary.averages.filesPerTransfer > 20 ? $scope.summary.averages.filesPerTransfer * 1.25 : 20),
        angleArc: 250,
        angleOffset: -125,
        fgColor: $scope.defaultColors[1]
      }
    },
    transfersPerUser: {
      value: $scope.summary.averages.transfersPerUser,
      options: {
        readOnly: true,
        thickness: 0.4,
        dynamicDraw: true,
        tickColorizeValues: true,
        min: 0,
        max: ($scope.summary.averages.transfersPerUser > 10 ? $scope.summary.averages.transfersPerUser * 1.25 : 10),
        angleArc: 250,
        angleOffset: -125,
        fgColor: $scope.defaultColors[2]
      }
    }
  };

  var usagePromise = agave.usage.data.getAll();

  usagePromise.then(
    function(response) {

      $scope.summary = response.data.result;
      $scope.summary.averages.transfersPerUser = Math.ceil($scope.summary.totals.transfers / $scope.summary.totals.users);
      $scope.summary.averages.filesPerUser = Math.ceil($scope.summary.totals.files / $scope.summary.totals.users);
      $scope.summary.averages.bytesPerTransfer = (parseFloat($scope.summary.averages.bytesPerTransfer) / 1073741824.0);
      $scope.knobs.bytesPerTransfer.options.max = ($scope.summary.averages.bytesPerTransfer > 100 ? $scope.summary.averages.bytesPerTransfer * 1.50 : 100);
      $scope.knobs.filesPerTransfer.options.max = ($scope.summary.averages.filesPerTransfer > 100 ? $scope.summary.averages.filesPerTransfer * 1.50 : 100);
      $scope.knobs.transfersPerUser.options.max = ($scope.summary.averages.transfersPerUser > 500? $scope.summary.averages.transfersPerUser * 1.50 : 500);

      $scope.hoursSaved = $scope.calculateHoursSaved();

      $scope.updateDataProtocolVsTransfers();
      $scope.updateDataProtocolVsSize();
      $scope.updateDataProtocolVsRetries();
      $scope.updateDataMap();
      $scope.updateDailyTransfers();
      $scope.updateHourlyTransfers();

      setTimeout(function() {

        $("#dataProtocolVsTransfers, #dataProtocolVsSize, #dataProtocolVsRetries").bind("plotclick", function (event, pos, item) {
          if (item) {
            $("#tooltip").html(item.series.label + " - " + $filter('number')(item.datapoint[0], 0) + '%')
            .css({top: pos.pageY+5, left: pos.pageX+5})
            .fadeIn(200);
          } else {
            $("#tooltip").hide();
          }
        });
      }, 200);
    },
    function(errorMessage) {
      toastr.error("Error retrieving stats", errorMessage);
    }
  );

  $scope.summary = {};
  $scope.hoursSaved = '';

  $scope.calculateHoursSaved = function() {
    var speedup = (parseFloat($scope.summary.averages.filesPerTransfer) - 1.0) / parseFloat($scope.summary.averages.filesPerTransfer);
    var secondsSaved = parseFloat($scope.summary.totals.transfers) * speedup * parseFloat($scope.summary.averages.transferTime);
    return secondsSaved / 3600.0;
  };

  $scope.updateDailyTransfers = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[0].values, function(value) {
      data.push([moment(value.name), value.count]);
    });
    $scope.dailyTransfers.data = [data];
    $scope.dailyTransfers.options = {
      xaxis: {
        mode: "time",
        tickSize: [30, 'day'],
        minTickSize: [1, "day"],
        timeformat: "%m/%d/%Y"
      },
      yaxis: {
        tickFormatter: function(n) {
          return $filter('humanNumber')(n, 0);
        }
      },
      grid: {
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateHourlyTransfers = function() {
    var data = [];
    angular.forEach($scope.summary.traffic[0].values, function(value) {
      data.push([moment("2014-01-01 " + value.name + ":00"), value.count]);
    });
    $scope.hourlyTransfers.data = [data];
    $scope.hourlyTransfers.options = {
      xaxis: {
        mode: "time",
        timeformat: "%I:00%p"
      },
      grid: {
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };



  $scope.updateDataProtocolVsTransfers = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[4].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.dataProtocolVsTransfers.data = data;
    $scope.dataProtocolVsTransfers.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateDataProtocolVsSize = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[5].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.dataProtocolVsSize.data = data;
    $scope.dataProtocolVsSize.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateDataProtocolVsRetries = function() {
    var data = [];
    angular.forEach($scope.summary.leaders[5].values, function(value, index) {
      data.push({label:value.name, data: value.count});
    });
    $scope.dataProtocolVsRetries.data = data;
    $scope.dataProtocolVsRetries.options = {
      colors: $scope.defaultColors,
      canvas: true,
      series: {
        pie: {
          show: true,
          combine: {
            color: '#999',
            threshold: 0.05
          },
        }
      },
      legend: {
        show: true
      },
      grid: {
        hoverable: true,
        clickable: true,
        backgroundColor: { colors: [ "#fff", "#fff" ] },
        borderWidth: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    };
  };

  $scope.updateDataMap = function() {
    var countries = {};
    angular.forEach($scope.summary.locations, function(location, ip) {
      var code = Commons.countries[location.country];
      if (code) {
        code = code.toLowerCase();
        if (countries[code]) {
          countries[code] += location.requests;
        } else {
          countries[code] = location.requests;
        }
      } else {
        console.log("Unable to resolve country " + location.country + " : " + location.requests + " requests");
      }

    });

    var max = 0,
    min = Number.MAX_VALUE,
    cc,
    startColor = [200, 238, 255],
    endColor = [0, 100, 145],
    colors = {},
    hex;

    //find maximum and minimum values
    for (cc in countries)
    {
      if (countries[cc] > max)
      {
        max = countries[cc];
      }
      if (countries[cc] < min)
      {
        min = countries[cc];
      }
    }

    //set colors according to values of GDP
    for (cc in countries) {
      if (countries[cc] > 0) {
        colors[cc] = '#';
        for (var i = 0; i<3; i++) {
          hex = Math.round(startColor[i] + (endColor[i] - startColor[i]) * (countries[cc] / (max - min))).toString(16);
          if (hex.length == 1) {
            hex = '0'+hex;
          }
          colors[cc] += (hex.length == 1 ? '0' : '') + hex;
        }
      }
    }

    $('#vmap').vectorMap({
      map: 'world_en',
      // colors: colors,
      backgroundColor: '#37455f',
      color: '#f3f3f3',
      hoverOpacity: 0.7,
      selectedColor: '#666666',
      // hoverColor: false,
      enableZoom: true,
      showTooltip: true,
      values: countries,
      scaleColors: ['#C8EEFF', '#006491'],
      normalizeFunction: 'polynomial',
      onLabelShow: function(event, label, code)
      {
        if (countries[code]) {
          label.text(label.text() + ' - ' + $filter('humanNumber')(countries[code], 0, true) );
        } else {
          event.preventDefault();
        }
      },
      onRegionClick: function(event, code) {
        if (!$(event.currentTarget).data('mapObject').isMoving) {
          e.preventDefault();
        }
      }
    });
  };

});
