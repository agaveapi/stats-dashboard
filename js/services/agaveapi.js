angular.module('agaveapi', []).service('agave', ['$rootScope','$window','md5', '$filter', '$http', function ($rootScope, $window, md5, $filter, $http) {

  var baseUrl = '//agaveapi.co/stats/v2/';
  //var baseUrl = '//docker.example.com:8099/stats/v2/';

  function appendTransform(defaults, transform) {

    // We can't guarantee that the default transformation is an array
    defaults = angular.isArray(defaults) ? defaults : [defaults];

    // Append the new transformation to the defaults
    return defaults.concat(transform);
  }

  function _getUsage(usageType) {
    return $http({
      url: baseUrl + usageType,
      method: 'GET'
    });
  }

  /* Top level object to query usage api. This does not require auth */
  this.usage = {

    apps: {
      getAll: function() {
        return _getUsage('apps');
      }
    },
    clients: {
      getAll: function() {
        return _getUsage('clients');
      }
    },
    code: {
      getAll: function() {
        return _getUsage('code');
      }
    },
    data: {
      getAll: function() {
        return _getUsage('data');
      }
    },
    jobs: {
      getAll: function() {
        return _getUsage('jobs');
      }
    },
    operations: {
      getAll: function() {
        return _getUsage('operations');
      }
    },
    summary: {
      getAll: function() {
        return _getUsage('summary');
      }
    },
    traffic: {
      getAll: function() {
        return _getUsage('traffic');
      }
    },
    users: {
      getAll: function() {
        return _getUsage('users');
      }
    },
  };


}])
// .filter('contains', ['Commons', function (Commons) {
//   return function (haystack, needle) {
//     return Commons.contains(haystack, needle);
//   };
// }])
;
