angular.module('commons.ng', []).service('Commons', ['$rootScope','$window','md5', '$filter', function ($rootScope, $window, md5, $filter) {

  this.isEmpty = function (item) {
    return !this.isNotEmpty(item);
  };

  this.isNotEmpty = function (item) {
    if (typeof item !== 'undefined' && item !== null && item !== '') {
      if (item.constructor === Array) {
        return item.length > 0;
      } else if (item instanceof Object){
        return Object.keys(item).length > 0;
      } else {
        return true;
      }
    } else {
      return false;
    }
  };

  this.isString = function(stringVal) {
    return this.isNotEmpty(stringVal) && (typeof stringVal) === 'string';
  };

  this.isEmptyValueInArray = function(someArray) {
    for (var i in someArray) {
      if (!someArray[i]) {
        return true;
      }
    }
    return false;
  };

  this.formatPhoneNumber = function(phoneNumber) {
    var formattedPhoneNumber = '';
    if (this.isNotEmpty(phoneNumber)) {
      formattedPhoneNumber = phoneNumber.replace(/[^0-9]/g, '');
      formattedPhoneNumber = formattedPhoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    }

    return formattedPhoneNumber;
  };

  /* Returns integer between min and max inclusive. */
  this.randomInt = function(min, max) {
    return (Math.floor(Math.random() * (max - min + 1)) + min);
  };

  this.randomDateString = function(year, month, day) {
    if (!year) {
      year = "2014";
    }
    if (!month) {
      month = this.randomInt(1,12);
      if (month < 10) {
        month = "0" + month;
      }
    }
    if (!day) {
      day = this.randomInt(1,28);
      if (day < 10) {
        day = "0" + day;
      }
    }
    return year + "-" + month + "-" + day + "T" + day + ":" + month + ":" + day + ".000Z";
  };

  /* Scrambles whatever you pass it and returns an array of shuffled values. */
  this.shuffle = function shuffle(o) {
    if (angular.isArray(o)) {
      for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
    } else if (angular.isObject(o)) {
      return this.shuffle(this.toArray(o));
    } else {
      return this.shuffle(o.toString().split(''));
    }
  };

  this.toArray = function(obj) {
    if (angular.isArray(obj)) {
      return obj;
    } else if (angular.isObject(obj)) {
      var arr = [];
      for(var m in obj) {
        if (obj.hasOwnProperty(m)) {
          arr.push(obj[m]);
        }
      }
      return arr;
    } else if (this.isNotEmpty(obj)) {
      return obj.toString().split();
    } else {
      return [];
    }
  };

  // Returns the set of the two arrays. Arrays may contain anything
  this.unique = function(array1, array2) {
    var a = this.toArray(array1).concat(this.toArray(array2));
    for(var i=0; i<a.length; ++i) {
      for(var j=i+1; j<a.length; ++j) {
        if(angular.equals(a[i],a[j]))
          a.splice(j--, 1);
      }
    }

    return a;
  };

  // Returns the first value of an array or object. Returns the first character
  // of a string, or first number of a numerical value, t for true, f for false.
  // note: object parameter ordering is not consistent, so this function is not
  // guaranteed to return the same result every time when called on an object.
  this.pop = function(objOrArray) {
    if (this.isNotEmpty(objOrArray)) {
      if (angular.isArray(objOrArray)) {
        return objOrArray[0];
      } else if (angular.isObject(objOrArray)) {
        return objOrArray[Object.keys(objOrArray)[0]];
      } else {
        return objOrArray.toString().substring(0,1);
      }
    }
  };

  /* Picks count values from objOrArray at random and returs the in an array. */
  this.pick = function(count, objOrArray) {
    var picks = [];
    if (this.isNotEmpty(objOrArray)) {
      var shuffledObjOrArray = this.shuffle(objOrArray);
      picks = shuffledObjOrArray.slice(0,Math.min(count, shuffledObjOrArray.length));
    }

    return picks;
  };

  /* capitalizes the first letter of a string */
  this.capitalize = function(s) {
    if (s) {
      s = s[0].toUpperCase() + s.slice(1);
    }

    return s;
  };

  this.camelcaseToSentence = function(s) {
    if (s) {
      return s.replace(/^[a-z]|[A-Z]/g, function(v, i) {
        return i === 0 ? v.toUpperCase() : " " + v.toLowerCase();
      });
    }
    return this.capitalize(s);
  };

  /* Returns true if the needle exists in the haystack. Equality is tested
     using angular.equals. Needle and haystack may be anything.
   */
  this.contains = function(haystack, needle) {
    if (this.isEmpty(haystack)) return false;
    if (this.isEmpty(needle)) return false;
    if (angular.isArray(haystack)) {
      for(var i in haystack) {
        if (angular.equals(haystack[i], needle)) return true;
      }
      return false;
    } else if (angular.isObject(haystack)) {
      return (this.isNotEmpty($filter('filter')(haystack, needle, 'static')));
    } else {
      return (haystack.toString().indexOf(needle) !== -1);
    }
  };

  /* Generates a gravitar from the given email address. Defaults to 'Mystery Man'
     if no gravitar exists for the email address.
   */
  this.getGravitarFromEmail = function(email) {
    var hash = md5.createHash(email || '');
    return 'http://www.gravatar.com/avatar/' + hash + '.jpg?d=mm';
  };

  this.formatAddress = function(address) {
    var saddress = '<address>';

    if (this.isNotEmpty(address)) {
      var isFirst = true;
      if (this.isNotEmpty(address.poBox)) {
        saddress += '<span>PO Box ' + address.poBox + '</span>';
        isFirst = false;
      }
      if (this.isNotEmpty(address.street1)) {
        saddress += (isFirst ? '' : '<br/>') + '<span>' + address.street1 + '</span>';
        isFirst = false;
      }

      if (this.isNotEmpty(address.street2)) {
        saddress += (isFirst ? '' : '<br/>') + '<span>' + address.street2 + '</span>';
        isFirst = false;
      }

      if (this.isNotEmpty(address.city) || this.isNotEmpty(address.state) || this.isNotEmpty(address.zip)) {

        if (this.isNotEmpty(address.city)) {
          saddress += (isFirst ? '' : '<br/>') + '<span>' + address.city;
          if (this.isNotEmpty(address.state)) {
            saddress += ', ' + address.state + '  ';
          }
        } else if (this.isNotEmpty(address.state)) {
          saddress += (isFirst ? '' : '<br/>') + '<span>' + address.state + '  ';
        }

        if (this.isNotEmpty(address.zip)) {
          saddress += address.zip;
        }

        if (saddress !== '') {
          saddress += '</span>';
        }
      }
    }

    return saddress + '</address>';

  };

  /* Sums values of "prop" field in each object contained within
     objectOrArray, where objectOrArray is an object or array. If
     prop is emptyish, this function attempts to sum the values
     of the array or object.
   */
  this.sum = function(objectOrArray, prop){
    if (this.isNotEmpty(objectOrArray)) {
      if (objectOrArray.constructor === Array) {
        return objectOrArray.reduce( function(a, b){
          if (prop) {
            return a + parseFloat(b[prop]);
          } else {
            return a + parseFloat(b);
          }
        }, 0.0);
      } else {
        return this.sum(this.toArray(objectOrArray));
      }
    } else {
      return 0;
    }
  };

  this.booleanToYesNo = function(value) {
    if (this.isEmpty(value)) {
      return 'No';
    } else if (value === 1 || value === '1' || value === true ){
      return 'Yes';
    } else {
      return 'No';
    }
  };

  this.booleanToTrueFalse = function(value) {
    if (this.isEmpty(value)) {
      return 'False';
    } else if (value === 1 || value === '1' || value === true ){
      return 'True';
    } else {
      return 'False';
    }
  };

  this.htmlEncode = function (value) {
    return value
      .replace(/&/g, '&amp;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#39;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
  };

  this.htmlDecode = function (value) {
    return value
    .replace(/&amp;/g, '&')
    .replace(/&quot;/g, '"')
    .replace(/&#39;/g, '')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>');
  };

  var dict = {};
  this.setTransient = function(key, value) {
    dict[key] = value;
  };

  this.getTransient = function(key) {
    if (key in dict) {
      return dict[key];
    } else {
      return null;
    }
  };

  this.removeTransient = function(key) {
    delete(dict[key]);
  };

  this.clearTransients = function() {
    dict = {};
  };

  this.countries = {
    'Afghanistan': 'AF',
    'Albania': 'AL',
    'Algeria': 'DZ',
    'American Samoa': 'AS',
    'Andorra': 'AD',
    'Angola': 'AO',
    'Anguilla': 'AI',
    'Antarctica': 'AQ',
    'Antigua And Barbuda': 'AG',
    'Argentina': 'AR',
    'Armenia': 'AM',
    'Aruba': 'AW',
    'Australia': 'AU',
    'Austria': 'AT',
    'Azerbaijan': 'AZ',
    'Bahamas': 'BS',
    'Bahrain': 'BH',
    'Bangladesh': 'BD',
    'Barbados': 'BB',
    'Belarus': 'BY',
    'Belgium': 'BE',
    'Belize': 'BZ',
    'Benin': 'BJ',
    'Bermuda': 'BM',
    'Bhutan': 'BT',
    'Bolivia': 'BO',
    'Bosnia And Herzegowina': 'BA',
    'Botswana': 'BW',
    'Bouvet Island (Norway) ': 'BV',
    'Brazil': 'BR',
    'British Indian Ocean Territory': 'IO',
    'Brunei Darussalam': 'BN',
    'Bulgaria': 'BG',
    'Burkina Faso': 'BF',
    'Burundi': 'BI',
    'Cambodia': 'KH',
    'Cameroon': 'CM',
    'Canada': 'CA',
    'Cape Verde': 'CV',
    'Cayman Islands': 'KY',
    'Central African Republic': 'CF',
    'Chad': 'TD',
    'Chile': 'CL',
    'China': 'CN',
    'Christmas Island': 'CX',
    'Cocos (Keeling) Islands (Austrailia)': 'CC',
    'Colombia': 'CO',
    'Comoros': 'KM',
    'Congo': 'CG',
    'Congo, The Drc': 'CD',
    'Cook Islands': 'CK',
    'Costa Rica': 'CR',
    'Cote D\'ivoire': 'CI',
    'Croatia (Local Name: Hrvatska)': 'HR',
    'Cuba': 'CU',
    'Cyprus': 'CY',
    'Czech Republic': 'CZ',
    'Denmark': 'DK',
    'Djibouti': 'DJ',
    'Dominica': 'DM',
    'Dominican Republic': 'DO',
    'East Timor': 'TP',
    'Ecuador': 'EC',
    'Egypt': 'EG',
    'El Salvador': 'SV',
    'Equatorial Guinea': 'GQ',
    'Eritrea': 'ER',
    'Estonia': 'EE',
    'Ethiopia': 'ET',
    'Falkland Islands (Malvinas)': 'FK',
    'Faroe Islands': 'FO',
    'Fiji': 'FJ',
    'Finland': 'FI',
    'France': 'FR',
    'France, Metropolitan': 'FX',
    'French Guiana': 'GF',
    'French Polynesia': 'PF',
    'French Southern Territories': 'TF',
    'Gabon': 'GA',
    'Gambia': 'GM',
    'Georgia': 'GE',
    'Germany': 'DE',
    'Ghana': 'GH',
    'Gibraltar': 'GI',
    'Greece': 'GR',
    'Greenland': 'GL',
    'Grenada': 'GD',
    'Guadeloupe': 'GP',
    'Guam': 'GU',
    'Guatemala': 'GT',
    'Guinea': 'GN',
    'Guinea-Bissau': 'GW',
    'Guyana': 'GY',
    'Haiti': 'HT',
    'Heard And Mc Donald Islands': 'HM',
    'Holy See (Vatican City State)': 'VA',
    'Honduras': 'HN',
    'Hong Kong': 'HK',
    'Hungary': 'HU',
    'Iceland': 'IS',
    'India': 'IN',
    'Indonesia': 'ID',
    'Iran (Islamic Republic of)': 'IR',
    'Iraq': 'IQ',
    'Ireland': 'IE',
    'Israel': 'IL',
    'Italy': 'IT',
    'Jamaica': 'JM',
    'Japan': 'JP',
    'Jordan': 'JO',
    'Kazakhstan': 'KZ',
    'Kenya': 'KE',
    'Kiribati': 'KI',
    'Korea, D.P.R.O.': 'KP',
    'Korea, Republic of': 'KR',
    'Kuwait': 'KW',
    'Kyrgyzstan': 'KG',
    'Laos': 'LA',
    'Latvia': 'LV',
    'Lebanon': 'LB',
    'Lesotho': 'LS',
    'Liberia': 'LR',
    'Libyan Arab Jamahiriya': 'LY',
    'Liechtenstein': 'LI',
    'Lithuania': 'LT',
    'Luxembourg': 'LU',
    'Macau': 'MO',
    'Macedonia': 'MK',
    'Madagascar': 'MG',
    'Malawi': 'MW',
    'Malaysia': 'MY',
    'Maldives': 'MV',
    'Mali': 'ML',
    'Malta': 'MT',
    'Marshall Islands': 'MH',
    'Martinique': 'MQ',
    'Mauritania': 'MR',
    'Mauritius': 'MU',
    'Mayotte': 'YT',
    'Mexico': 'MX',
    'Micronesia, Federated States of': 'FM',
    'Moldova, Republic of': 'MD',
    'Monaco': 'MC',
    'Mongolia': 'MN',
    'Montenegro': 'ME',
    'Montserrat': 'MS',
    'Morocco': 'MA',
    'Mozambique': 'MZ',
    'Myanmar (Burma)': 'MM',
    'Namibia': 'NA',
    'Nauru': 'NR',
    'Nepal': 'NP',
    'Netherlands': 'NL',
    'Netherlands Antilles': 'AN',
    'New Caledonia': 'NC',
    'New Zealand': 'NZ',
    'Nicaragua': 'NI',
    'Niger': 'NE',
    'Nigeria': 'NG',
    'Niue': 'NU',
    'Norfolk Island': 'NF',
    'Northern Mariana Islands': 'MP',
    'Norway': 'NO',
    'Oman': 'OM',
    'Pakistan': 'PK',
    'Palau': 'PW',
    'Panama': 'PA',
    'Papua New Guinea': 'PG',
    'Paraguay': 'PY',
    'Peru': 'PE',
    'Philippines': 'PH',
    'Pitcairn': 'PN',
    'Poland': 'PL',
    'Portugal': 'PT',
    'Puerto Rico': 'PR',
    'Qatar': 'QA',
    'Reunion': 'RE',
    'Romania': 'RO',
    'Russian Federation': 'RU',
    'Rwanda': 'RW',
    'Saint Kitts And Nevis': 'KN',
    'Saint Lucia': 'LC',
    'Saint Vincent And The Grenadines': 'VC',
    'Samoa': 'WS',
    'San Marino': 'SM',
    'Sao Tome And Principe': 'ST',
    'Saudi Arabia': 'SA',
    'Senegal': 'SN',
    'Serbia': 'RS',
    'Seychelles': 'SC',
    'Sierra Leone': 'SL',
    'Singapore': 'SG',
    'Slovakia (Slovak Republic)': 'SK',
    'Slovenia': 'SI',
    'Solomon Islands': 'SB',
    'Somalia': 'SO',
    'South Africa': 'ZA',
    'South Sudan': 'SS',
    'South Georgia And South S.S.': 'GS',
    'Spain': 'ES',
    'Sri Lanka': 'LK',
    'St. Helena': 'SH',
    'St. Pierre And Miquelon': 'PM',
    'Sudan': 'SD',
    'Suriname': 'SR',
    'Svalbard And Jan Mayen Islands': 'SJ',
    'Swaziland': 'SZ',
    'Sweden': 'SE',
    'Switzerland': 'CH',
    'Syrian Arab Republic': 'SY',
    'Taiwan, Province of China': 'TW',
    'Tajikistan': 'TJ',
    'Tanzania, United Republic of': 'TZ',
    'Thailand': 'TH',
    'Togo': 'TG',
    'Tokelau': 'TK',
    'Tonga': 'TO',
    'Trinidad And Tobago': 'TT',
    'Tunisia': 'TN',
    'Turkey': 'TR',
    'Turkmenistan': 'TM',
    'Turks and Caicos Islands': 'TC',
    'Tuvalu': 'TV',
    'Uganda': 'UG',
    'Ukraine': 'UA',
    'United Arab Emirates': 'AE',
    'United Kingdom': 'GB',
    'United States': 'US',
    'U.S. Minor Islands': 'UM',
    'Uruguay': 'UY',
    'Uzbekistan': 'UZ',
    'Vanuatu': 'VU',
    'Venezuela': 'VE',
    'Vietnam': 'VN',
    'Virgin Islands (British)': 'VG',
    'Virgin Islands (U.S.)': 'VI',
    'Wallis and Futuna Islands': 'WF',
    'Western Sahara': 'EH',
    'Yemen': 'YE',
    'Zambia': 'ZM',
    'Zimbabwe ': 'ZW',
  };

  this.getCountryByCode = function(code) {
    for (i in this.countries) {
      if (this.countries[i].toLowerCase() === code.toLowerCase()) {
        return this.countries[i];
      }
    }

    return null;
  };

  this.states = [
    {
      "name": "Alabama",
      "abbreviation": "AL"
    },
    {
      "name": "Alaska",
      "abbreviation": "AK"
    },
    {
      "name": "Arizona",
      "abbreviation": "AZ"
    },
    {
      "name": "Arkansas",
      "abbreviation": "AR"
    },
    {
      "name": "California",
      "abbreviation": "CA"
    },
    {
      "name": "Colorado",
      "abbreviation": "CO"
    },
    {
      "name": "Connecticut",
      "abbreviation": "CT"
    },
    {
      "name": "Delaware",
      "abbreviation": "DE"
    },
    {
      "name": "District Of Columbia",
      "abbreviation": "DC"
    },
    {
      "name": "Florida",
      "abbreviation": "FL"
    },
    {
      "name": "Georgia",
      "abbreviation": "GA"
    },
    {
      "name": "Hawaii",
      "abbreviation": "HI"
    },
    {
      "name": "Idaho",
      "abbreviation": "ID"
    },
    {
      "name": "Illinois",
      "abbreviation": "IL"
    },
    {
      "name": "Indiana",
      "abbreviation": "IN"
    },
    {
      "name": "Iowa",
      "abbreviation": "IA"
    },
    {
      "name": "Kansas",
      "abbreviation": "KS"
    },
    {
      "name": "Kentucky",
      "abbreviation": "KY"
    },
    {
      "name": "Louisiana",
      "abbreviation": "LA"
    },
    {
      "name": "Maine",
      "abbreviation": "ME"
    },
    {
      "name": "Maryland",
      "abbreviation": "MD"
    },
    {
      "name": "Massachusetts",
      "abbreviation": "MA"
    },
    {
      "name": "Michigan",
      "abbreviation": "MI"
    },
    {
      "name": "Minnesota",
      "abbreviation": "MN"
    },
    {
      "name": "Mississippi",
      "abbreviation": "MS"
    },
    {
      "name": "Missouri",
      "abbreviation": "MO"
    },
    {
      "name": "Montana",
      "abbreviation": "MT"
    },
    {
      "name": "Nebraska",
      "abbreviation": "NE"
    },
    {
      "name": "Nevada",
      "abbreviation": "NV"
    },
    {
      "name": "New Hampshire",
      "abbreviation": "NH"
    },
    {
      "name": "New Jersey",
      "abbreviation": "NJ"
    },
    {
      "name": "New Mexico",
      "abbreviation": "NM"
    },
    {
      "name": "New York",
      "abbreviation": "NY"
    },
    {
      "name": "North Carolina",
      "abbreviation": "NC"
    },
    {
      "name": "North Dakota",
      "abbreviation": "ND"
    },
    {
      "name": "Ohio",
      "abbreviation": "OH"
    },
    {
      "name": "Oklahoma",
      "abbreviation": "OK"
    },
    {
      "name": "Oregon",
      "abbreviation": "OR"
    },
    {
      "name": "Pennsylvania",
      "abbreviation": "PA"
    },
    {
      "name": "Rhode Island",
      "abbreviation": "RI"
    },
    {
      "name": "South Carolina",
      "abbreviation": "SC"
    },
    {
      "name": "South Dakota",
      "abbreviation": "SD"
    },
    {
      "name": "Tennessee",
      "abbreviation": "TN"
    },
    {
      "name": "Texas",
      "abbreviation": "TX"
    },
    {
      "name": "Utah",
      "abbreviation": "UT"
    },
    {
      "name": "Vermont",
      "abbreviation": "VT"
    },
    {
      "name": "Virginia",
      "abbreviation": "VA"
    },
    {
      "name": "Washington",
      "abbreviation": "WA"
    },
    {
      "name": "West Virginia",
      "abbreviation": "WV"
    },
    {
      "name": "Wisconsin",
      "abbreviation": "WI"
    },
    {
      "name": "Wyoming",
      "abbreviation": "WY"
    }
  ];


}])
.filter('length', [ 'Commons', function(Commons) {
  function lengthFilter(value) {
    if (value !== null) {
      return value.length;
    } else {
      return 0;
    }
  }

  return lengthFilter;
}])
.filter('capitalize', [ 'Commons', function(Commons) {
  function capitalizeFilter(value) {
    return Commons.capitalize(value);
  }

  return capitalizeFilter;
}])
.filter('booleanToYesNo', [ 'Commons', function(Commons) {
  function booleanToYesNoFilter(value) {
    return Commons.booleanToYesNo(value);
  }

  return booleanToYesNoFilter;
}])
.filter('booleanToTrueFalse', [ 'Commons', function(Commons) {
  function booleanToTrueFalseFilter(value) {
    return Commons.booleanToTrueFalse(value);
  }

  return booleanToTrueFalseFilter;
}])
.filter('formatPhoneNumber', ['Commons', function(Commons) {
  function formatPhoneFilter(phoneNumber) {
    return Commons.formatPhoneNumber(phoneNumber);
  }

  return formatPhoneFilter;
}])
.filter('formatAddress', ['Commons', function(Commons) {
  function formatAddressFilter(address) {
    return Commons.formatAddress(address);
  }

  return formatAddressFilter;
}])
.filter('gravitar', ['Commons', function(Commons) {

  function gravitarFilter(email) {
    var gravitar = null;
    if (email) {
      gravitar = Commons.getGravitarFromEmail(email);
    }

    return gravitar;
  }

  return gravitarFilter;
}])
.filter('findName', ['Commons', function(Commons) {

  function findNameFilter(obj) {
    var formattedName = '';
    if (Commons.isNotEmpty(obj)) {
      if (Commons.isNotEmpty(obj.details)) {
        if (Commons.isNotEmpty(obj.details.firstName) || Commons.isNotEmpty(obj.details.lastName)) {
          formattedName = obj.details.firstName + ' ' + obj.details.lastName;
        } else if (Commons.isNotEmpty(obj.details.name)) {
          formattedName = obj.details.name;
        }
      } else if (Commons.isNotEmpty(obj.name)) {
        formattedName = obj.name;
      }
    }

    return formattedName;
  }

  return findNameFilter;
}])
.filter('formatStarRating', ['Commons', function(Commons) {
  function formatStarRatingFilter(rating) {
    var sRating = '';
    if (Commons.isNotEmpty(rating)) {
      rating = parseFloat(rating);
      for (var i=0; i<Math.floor(rating); i++) {
        sRating += '<i class="fa fa-star text-yellow fa-fw"></i>';
      }
      var partialRating = rating - Math.floor(rating);
      if (partialRating > 0.5) {
        sRating += '<i class="fa fa-star-half-o text-yellow fa-fw"></i>';
      } else if (partialRating > 0.25) {
        sRating += '<i class="fa fa-star-half text-yellow fa-fw"></i>';
      } else if (partialRating > 0) {
        sRating += '<i class="fa fa-star-o text-yellow fa-fw"></i>';
      }
    }

    return sRating;
  }

  return formatStarRatingFilter;
}])
.filter('contains', ['Commons', function (Commons) {
  return function (haystack, needle) {
    return Commons.contains(haystack, needle);
  };
}])
.filter('camelcaseToSentence', ['Commons', function (Commons) {
  return function (s) {
    return Commons.camelcaseToSentence(s);
  };
}])
.filter('excerpt', function () {
  return function (value, wordwise, max, tail) {
    if (!value) return '';

    max = parseInt(max, 10);
    if (!max) return value;
    if (value.length <= max) return value;

    value = value.substr(0, max);
    if (wordwise) {
      var lastspace = value.lastIndexOf(' ');
      if (lastspace != -1) {
        value = value.substr(0, lastspace);
      }
    }

    return value + (tail || ' ...');
  };
})
.filter('humanSize', function () {
  return function (size, precision, plainText) {

    if (precision == 0 || precision == null){
      precision = 1;
    }
    if (size == 0 || size == null){
      return "";
    }
    else if(!isNaN(size)){
      var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'XB'];
      var posttxt = 0;

      if (size < 1024) {
        if (plainText) {
          return Number(size) + sizes[posttxt];
        } else {
          return Number(size) + '<font class="units">' + sizes[posttxt] + '</font>';
        }
      }
      while( size >= 1024 ) {
        posttxt++;
        size = size / 1024;
      }

      var power = Math.pow (10, precision);
      var poweredVal = Math.ceil (size * power);

      size = poweredVal / power;

      if (plainText) {
        return size + sizes[posttxt];
      } else {
        return size + '<font class="units">' + sizes[posttxt] + '</font>';
      }
    }else{
      console.log('Error: Not a number.' + size + ' ' + precision);
      return 0;
    }

  };
})
.filter('humanNumber', function () {
  return function (size, precision, plainText) {

    if (precision == 0 || precision == null){
      precision = 1;
    }
    if (size == 0 || size == null){
      return "";
    }
    else if(!isNaN(size)){
      var sizes = ['', 'K', 'M', 'G', 'T', 'P', 'X'];
      var posttxt = 0;

      if (size < 1024) {
        return Number(size);
      }
      while( size >= 1024 ) {
        posttxt++;
        size = size / 1024;
      }

      var power = Math.pow (10, precision);
      var poweredVal = Math.ceil (size * power);

      size = poweredVal / power;

      if (plainText) {
        return size + sizes[posttxt];
      } else {
        return size + '<font class="units">' + sizes[posttxt] + '</font>';
      }
    }else{
      console.log('Error: Not a number.' + size + ' ' + precision);
      return 0;
    }

  };
});
