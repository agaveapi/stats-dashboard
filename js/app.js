var App = angular.module('AgaveDashboard', ['ngRoute', 'ui.router', 'ngMd5', 'ngMap',
  'ngSanitize', 'ui.bootstrap', 'ui.knob', 'oc.lazyLoad', 'LocalStorageModule', 'angularMoment',
  'angular-flot', 'toastr', 'commons.ng', 'agaveapi',
]);

App.constant('angularMomentConfig', {
  preprocess: 'utc', // optional
  timezone: 'America/Chicago' // optional
});

App.config(function($stateProvider, $urlRouterProvider, localStorageServiceProvider, toastrConfig) {

  // For any unmatched url, redirect to /
  $urlRouterProvider.otherwise("/index");

  localStorageServiceProvider
    .setPrefix('agaveDashboard')
    .setStorageType('localStorage')
    .setNotify(true, true);

  //
  // Now set up the states
  $stateProvider
    .state('summary', {
      url: "/index",
      templateUrl: 'template/states/summary.html',
      controller: 'SummaryController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: []
          });
        }]
      }
    })
    .state('traffic', {
      url: "/traffic",
      templateUrl: 'template/states/traffic.html',
      controller: 'TrafficController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: []
          });
        }]
      }
    })
    .state('users', {
      url: "/users",
      templateUrl: 'template/states/users.html',
      controller: 'UsersController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: []
          });
        }]
      }
    })
    .state('jobs', {
      url: "/jobs",
      templateUrl: 'template/states/jobs.html',
      controller: 'JobsController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: [
              'vendors/jqvmap/jqvmap.css',
              'vendors/jqvmap/jquery.vmap.min.js',
              'vendors/jqvmap/maps/jquery.vmap.world.js',
            ]
          });
        }]
      }
    })
    .state('apps', {
      url: "/apps",
      templateUrl: 'template/states/apps.html',
      controller: 'AppsController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: []
          });
        }]
      }
    })
    .state('data', {
      url: "/data",
      templateUrl: 'template/states/data.html',
      controller: 'DataController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: [
              'vendors/jqvmap/jqvmap.css',
              'vendors/jqvmap/jquery.vmap.min.js',
              'vendors/jqvmap/maps/jquery.vmap.world.js',
            ]
          });
        }]
      }
    })
    .state('code', {
      url: "/code",
      templateUrl: 'template/states/code.html',
      controller: 'CodeController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: []
          });
        }]
      }
    })
    .state('clients', {
      url: "/clients",
      templateUrl: 'template/states/clients.html',
      controller: 'ClientsController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: []
          });
        }]
      }
    })
    .state('operations', {
      url: "/operations",
      templateUrl: 'template/states/operations.html',
      controller: 'OperationsController',
      resolve: {
        loadMyCtrl: ['$ocLazyLoad', function($ocLazyLoad) {
          return $ocLazyLoad.load({
            files: [
              'vendors/jqvmap/jqvmap.css',
              'vendors/jqvmap/jquery.vmap.min.js',
              'vendors/jqvmap/maps/jquery.vmap.usa.js',
            ]
          });
        }]
      }
    });

  angular.extend(toastrConfig, {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-top-full-width",
    "onclick": null,
    "showDuration": "1000",
    "hideDuration": "3000",
    "timeOut": "3000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  });
});
