#Agave Dashboard

The Agave Dashboard is a AngularJS + Bootstrap app to visualize realtime usage information from the Agave Stats API. The app is entirely client side static assets that run in your browser. The Dashboard is driven by data available from unauthenticated calls to the Agave Stats API. The data presented is aggregated data from all of the public tenants in the hosted Agave Platform instance.

## Running

This repository is distributed as a Docker container. Simply start the container and expose port 80 system:

```  
$ docker run -p "80:80" agaveapi/stats-dashboard  
```  

There is a Docker Compose file included for convenience.

```  
$ docker-compose up -d  
```
